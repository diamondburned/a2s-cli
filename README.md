# a2s-cli

Finally, a way to query A2S without using PHP!

## How-to

- Build
	- `go get "github.com/rumblefrog/go-a2s"`
	- `go build main.go -o "a2s-cli"`

- Run
	- `./a2s-cli`
		- Argument 1
			- `info`
			- `player`
			- `rules`
		- Argument 2: Address
		- Options
			- `--debug` prints full JSON
			- `--timeout n` sets timeout to `n` milliseconds (default 5000)
- Example
    - `./a2s-cli --timeout 5000 info "127.0.0.1:27015"`
## Credits

[**Fishy's A2S library in Go**](https://github.com/rumblefrog/go-a2s)
