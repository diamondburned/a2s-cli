package main

import (
	a2s "github.com/rumblefrog/go-a2s"
	"fmt"
	"os"
	"flag"
	"encoding/json"
	"time"
)

func readArgs() (bool, int64, []string) {
	var Debug bool
	flag.BoolVar(&Debug, "debug", false, "Prints a fully marshalled JSON output")

	var Time int64
	flag.Int64Var(&Time, "time", 5000, "The timeout for A2S in ms (default 5000)")

	flag.Parse()

	return Debug, Time, flag.Args()
}

var Debug, Time, args = readArgs()

func chk(err error) {
	if err != nil {
		fmt.Println("Error!\n", err)
		os.Exit(1)
	}
}

func printLines(strs ...string) {
	var output string

	for _, line := range strs {
		output += line + "\n" 
	}

	fmt.Println(output)
}

func boolToYes(boolean bool) string {
	switch boolean {
	case true:
		return "Yes"
	default:
		return "No"
	}
}

func debugMarshal(query interface{}) {
	if Debug {
		JSON, err := json.Marshal(query)
		chk(err)

		fmt.Println(string(JSON))
	}
}

func main() {
	if len(args) < 2 {
		fmt.Printf("%s\n%s\n",
			"No argument provided.",
			"Allowed arguments: `info', `player', `rules'.")
		
		os.Exit(0)
	}

	duration := time.Duration(Time) * time.Millisecond

	client, err := a2s.NewClient(args[1], a2s.TimeoutOption(duration))
	chk(err)

	defer client.Close()

	switch args[0] {
	case "info":
		query, err := client.QueryInfo()
		chk(err)

		printLines("Output:",
			"Name: " + query.Name,
			"Map: " + query.Map,
			"Game: " + query.Game,
			"Version: " + query.Version,
			"AppID: " + fmt.Sprintf("%v", query.ID),
			"Player count: " + fmt.Sprintf("%v/%v (%v bots)", query.Players, query.MaxPlayers, query.Bots),
			"OS: " + query.ServerOS.String(),
			"Password: " + boolToYes(query.Visibility),
			"VAC Protected: " + boolToYes(query.VAC))
	
		debugMarshal(query)
	case "player":
		query, err := client.QueryPlayer()
		chk(err)

		var players string
		for _, player := range query.Players {
			players += fmt.Sprintf("\t%v: %s\n", player.Index, player.Name)
			players += fmt.Sprintf("\t\t- Score: %v\n\t\t- Duration: %v\n", player.Score, player.Duration)
		}

		printLines("Output:",
			"Player count: " + fmt.Sprintf("%v", query.Count),
			"Players:", players)

		debugMarshal(query)
	case "rules":
		query, err := client.QueryRules()
		chk(err)

		var rules string
		for name, desc := range query.Rules {
			rules += fmt.Sprintf("\t%s:\"%s\"\n", name, desc)
		}

		printLines("Output:",
			"Rule count: " + fmt.Sprintf("%v", query.Count),
			"Rules: ", rules)

		debugMarshal(query)
	default:
		fmt.Printf("%s\n%s",
			"No argument provided.",
			"Allowed arguments: `info', `player', `rules'.")
		
		client.Close()
		os.Exit(0)
	}	
}